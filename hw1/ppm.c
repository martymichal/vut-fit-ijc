/* ppm.c
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 07/03/2020
 * Preloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm.h"
#include "error.h"

#define BASIC_LINE_SIZE 10

FILE *fp = NULL;
char *line = NULL;

struct ppm * ppm_read(const char * filename)
{
  unsigned xsize = 0, ysize = 0;

  // Open the file
  fp = fopen(filename, "r");
  if (fp == NULL) {
    error_exit("ppm_read: There was a problem while opening file %s\n", filename);
  }
  atexit(close_file);

  // Read it
  enum state {label, width, height, color} current_state = label;
  int line_size = sizeof(char) * BASIC_LINE_SIZE;
  line = malloc(line_size);
  char *lastchar = NULL, *delimited = NULL, tmp[BASIC_LINE_SIZE], delimiters[] = "\n\t\r ";
  atexit(free_line);
  while (current_state <= 3)
  {
    if (fgets(tmp, line_size, fp) == NULL)
    {
      warning_msg("ppm_read: There was a problem while reading file %s\n", filename);
      return NULL;
    }

    strcpy(line, tmp);

    if (line[strlen(line)-1] != '\n')
    {
      if (realloc(line, line_size * 2) == NULL) {
        warning_msg("ppm_read: There was a problem while reallocing\n");
        return NULL;
      }
      continue;
    }

    delimited = strtok(line, delimiters);
    while (delimited != NULL)
    {
      switch (current_state)
      {
        case label:
          if (strcmp(delimited, "P6") != 0)
          {
            warning_msg("ppm_read: The format of the image is not P6\n");
            return NULL;
          }
          break;
        case width:
          xsize = strtoul(delimited, &lastchar, 10);
          if (*lastchar != '\0')
          {
            warning_msg("ppm_read: The value of image width is invalid\n");
            return NULL;
          }
          break;
        case height:
          ysize = strtoul(delimited, &lastchar, 10);
          if (*lastchar != '\0')
          {
            warning_msg("ppm_read: The value of image height is invalid\n");
            return NULL;
          }
          break;
        case color:
          strtoul(delimited, &lastchar, 10);
          if (*lastchar != '\0')
          {
            warning_msg("ppm_read: The value of image color is invalid\n");
            return NULL;
          }
          break;
      }
      current_state++;
      delimited = strtok(NULL, delimiters);
    }
  }

  // Allocate the right amount of memory
  struct ppm *picture = malloc(sizeof(struct ppm) + sizeof(char) * 3 * xsize * ysize);
  picture->xsize = xsize;
  picture->ysize = ysize;

  for (int i = 0; i < 3 * xsize * ysize; i++)
  {
    picture->data[i] = fgetc(fp);
  }

  return picture;
}

void ppm_free(struct ppm *p)
{
  free(p);
}

void close_file(void)
{
  fclose(fp);
}

void free_line(void)
{
  free(line);
}
