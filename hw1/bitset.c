/* bitset.c
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 29/02/2020
 * Preloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#include "bitset.h"

#ifdef USE_INLINE
extern bitset_t* bitset_alloc(int velikost);

extern void bitset_free(bitset_t* jmeno_pole);

extern bitset_index_t bitset_size(bitset_t* jmeno_pole);

extern void bitset_setbit(bitset_t* jmeno_pole, bitset_index_t index, int vyraz);

extern int bitset_getbit(bitset_t* jmeno_pole, bitset_index_t index);
#endif
