/* steg-decode.c
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 07/03/2020
 * Preloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#include <math.h>
#include "ppm.h"
#include "error.h"
#include "bitset.h"

int main(int argc, char *argv[])
{
  #ifndef USE_INLINE
  error_exit("steg-decode: Program requires compilation with the '-DUSE_INLINE' flag due to restrictions in usage of macros\n");
  #endif
  if (argc == 1)
    error_exit("Missing filename argument\n");

  struct ppm *picture = ppm_read(argv[1]);
  if (picture == NULL)
    error_exit("There was a problem while reading the provided file\n");

  bitset_t *data_bitlist = NULL;
  #ifdef USE_INLINE
  data_bitlist = bitset_alloc(picture->xsize * picture->ysize * 3);
  #endif

  // Find all primes starting with number 23
  for (bitset_index_t i = 2; i < sqrt(bitset_size(data_bitlist)); i++)
  {
    if (bitset_getbit(data_bitlist, i) == 0)
    {
      for (bitset_index_t j = i * i; j < bitset_size(data_bitlist); j += i)
      {
        // Number is not prime
        bitset_setbit(data_bitlist, j, 1);
      }
    }
  }

  char character = '\0';
  int pos = 0;
  for (bitset_index_t i = 23; i < bitset_size(data_bitlist); i++)
  {
    if (bitset_getbit(data_bitlist, i) == 0)
    {
      character |= (picture->data[i] & 1) << pos;
      pos++;
      if (pos != 8) continue;
      printf("%c", character);
      if (character == '\0') break;
      pos = 0, character = '\0';
    }
  }

  printf("\n");
  if (character != '\0')
    error_exit("steg-decode: The message was not terminated properly (missing '\\0' at the end)\n");
  bitset_free(data_bitlist);
  ppm_free(picture);
  return 0;
}
