/* primes.c
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 29/02/2020
 * Preloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#include <time.h>
#include "bitset.h"
#include "eratosthenes.h"

int main(int argc, char *argv[])
{
  clock_t start = clock();
  #ifdef USE_INLINE
  bitset_t* p = bitset_alloc(500000000);
  #else
	bitset_alloc(p, 500000000);
  #endif

	Eratosthenes(p);

	bitset_free(p);
  fprintf(stderr, "Time=%.3g\n", (double)(clock()-start)/CLOCKS_PER_SEC);
	return EXIT_SUCCESS;
}
