/* error.c
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 07/03/2020
 * Preloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#include "error.h"

void warning_msg(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  
  fprintf(stderr, "CHYBA: ");
  vfprintf(stderr, fmt, ap);
  va_end(ap);
}

void error_exit(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);

  fprintf(stderr, "CHYBA: ");
  vfprintf(stderr, fmt, ap);
  va_end(ap);

  exit(1);
}
