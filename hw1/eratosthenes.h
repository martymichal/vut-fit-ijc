/* eratosthenes.h
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 05/03/2020
 * Přeloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#ifndef ERATOSTHENES_H_INCLUDED
#define ERATOSTHENES_H_INCLUDED

#include "bitset.h"
#include <stdio.h>

void Eratosthenes(bitset_t *pole);

#endif
