/* eratosthenes.c
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 05/03/2020
 * Přeloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#include "eratosthenes.h"
#include <math.h>

void Eratosthenes(bitset_t *pole)
{
  for (bitset_index_t i = 2; i < sqrt(bitset_size(pole)); i++)
  {
    if (bitset_getbit(pole, i) == 0)
    {
      for (bitset_index_t j = i * i; j < bitset_size(pole); j += i)
      {
        bitset_setbit(pole, j, 1);
      }
    }
  }

  int count = 0;
  bitset_index_t primes[10];
  for (bitset_index_t i = bitset_size(pole) - 1; i > 0; i--)
  {
    if (bitset_getbit(pole, i) == 0)
    {
      primes[count] = i;
      count++;
    }
    if (count == 10)
      break;
  }

  for (; count > 0; count--)
  {
    printf("%lu\n", primes[count-1]);
  }
}
