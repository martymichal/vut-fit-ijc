/* ppm.h
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 07/03/2020
 * Preloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#include <stdlib.h>

struct ppm {
  unsigned xsize;
  unsigned ysize;
  char data[]; // RGB bajty, celkem 3*xsize*ysize
};

struct ppm * ppm_read(const char * filename);

void ppm_free(struct ppm *p);

void close_file(void);

void free_line(void);
