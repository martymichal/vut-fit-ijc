/* bitset.h
 * Řešení IJC-DU1, příklad A
 * Ondřej Míchal, FIT, 29/02/2020
 * Preloženo: gcc (GCC) 9.2.1 20190827 (Red Hat 9.2.1-1)
 */

#ifndef BITSET_H_INCLUDED
#define BITSET_H_INCLUDED

#include <assert.h>
#include <stdlib.h>
#include "error.h"

#define BITSET_BASE (sizeof(unsigned long) * __CHAR_BIT__)

typedef unsigned long bitset_t;
typedef unsigned long bitset_index_t;

#define bitset_create(jmeno_pole, velikost) \
  bitset_t *jmeno_pole[(((velikost) % BITSET_BASE) > 0 ? ((velikost) / BITSET_BASE + 2) : ((velikost) / BITSET_BASE)) + 1] = {(velikost)};\
  static_assert((velikost) >= 0, "bitset_create: Invalid size of bitset");

#ifndef USE_INLINE
#define bitset_alloc(jmeno_pole, velikost) \
  static_assert((velikost) >= 0, "bitset_alloc: Invalid size of bitset");\
  bitset_t* jmeno_pole = calloc((((velikost) % BITSET_BASE) > 0 ? ((velikost) / BITSET_BASE + 1) : ((velikost) / BITSET_BASE)) + 1, BITSET_BASE / __CHAR_BIT__);\
  if (jmeno_pole == NULL) error_exit("bitset_alloc: Chyba alokace paměti\n");\
  jmeno_pole[0] = (velikost)

#define bitset_free(jmeno_pole) free(jmeno_pole)

#define bitset_size(jmeno_pole) (bitset_index_t) jmeno_pole[0]

#define bitset_setbit(jmeno_pole, index, vyraz) (index >= 0 || index < bitset_size(jmeno_pole)) ? (vyraz == 0 ? (jmeno_pole[(index / BITSET_BASE) + 1] &= ~((bitset_index_t) 1 << (index % BITSET_BASE))) : (jmeno_pole[(index / BITSET_BASE) + (bitset_index_t) 1] |= ((bitset_index_t) 1 << (index % BITSET_BASE)))) : (error_exit("bitset_getbit: Index %lu mimo rozsah 0..%lu", (unsigned long)index, (unsigned long)bitset_size(jmeno_pole)), 1)

#define bitset_getbit(jmeno_pole, index) ((index >= 0 || index < bitset_size(jmeno_pole)) ? ((jmeno_pole[(index / BITSET_BASE) + (bitset_index_t) 1] & ((unsigned long) 1 << (index % BITSET_BASE))) && 1) : (error_exit("bitset_getbit: Index %lu mimo rozsah 0..%lu", (unsigned long)index, (unsigned long)bitset_size(jmeno_pole)), 1))
#endif

#ifdef USE_INLINE
inline bitset_t* bitset_alloc(int velikost) {
  bitset_t* pole = calloc((velikost % BITSET_BASE) > 0 ? (velikost / BITSET_BASE + 2) : (velikost / BITSET_BASE + 1) , BITSET_BASE / __CHAR_BIT__);
  if (pole == NULL) error_exit("bitset_alloc: Chyba alokace paměti\n");
  pole[0] = velikost;
  return pole;
}

inline void bitset_free(bitset_t* jmeno_pole) {
  free(jmeno_pole);
}

inline bitset_index_t bitset_size(bitset_t* jmeno_pole) {
  return (bitset_index_t) jmeno_pole[0];
}

inline void bitset_setbit(bitset_t* jmeno_pole, bitset_index_t index, int vyraz) {
  if (index < 0 || index >= bitset_size(jmeno_pole))
    error_exit("bitset_setbit: Index %lu mimo rozsah 0..%lu", (unsigned long)index, (unsigned long)bitset_size(jmeno_pole));
  if (vyraz == 0)
    jmeno_pole[(index / BITSET_BASE) + 1] &= ~((bitset_index_t) 1 << (index % BITSET_BASE));
  else
    jmeno_pole[(index / BITSET_BASE) + (bitset_index_t) 1] |= ((bitset_index_t) 1 << (index % BITSET_BASE));
}

inline int bitset_getbit(bitset_t* jmeno_pole, bitset_index_t index) {
  if (index < 0 || index >= bitset_size(jmeno_pole))
    error_exit("bitset_getbit: Index %lu mimo rozsah 0..%lu", (unsigned long)index, (unsigned long)bitset_size(jmeno_pole));
  return jmeno_pole[index / BITSET_BASE + (bitset_index_t) 1] & ((bitset_index_t) 1 << (index % BITSET_BASE)) && 1;
}
#endif

#endif
