#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <getopt.h>
#include <error.h>
#include <string.h>
#include <sys/stat.h>

#define PROGRAM_NAME "tail"
#define DEFAULT_N_LINES 10

void print_from_position(FILE *f, long *pos) {
  char buffer[BUFSIZ];
  fseek(f, *pos, SEEK_SET);
  while (!feof(f)) {
    strcpy(buffer, "");
    fread(buffer, 1, BUFSIZ, f);
    printf("%s", buffer);
  }
  *pos = ftell(f);
}

bool tail_file(FILE *f, int lines, bool from_top)
{
  char buffer[BUFSIZ], c;
  long start_pos, end_pos, pos;
  size_t char_num = 0, i;

  // If going from top then the requested number of newlines if n-1
  if (from_top) {
    // If the requested line is 0 or 1 then print the whole file
    if (lines == 0 || lines == 1) {
      pos = 0;
      print_from_position(f, &pos);
      return EXIT_SUCCESS;
    }
    lines--;
  }

  // Get to the end of the file (or stay at the beginning depending on the mode)
  start_pos = pos = ftell(f);
  fseek(f, 0, SEEK_END);
  end_pos = ftell(f);
  if (!from_top) {
    pos = end_pos;
  } else {
    fseek(f, start_pos, SEEK_SET);
  }

  // Start reading until hitting the right line
  // Move the head position back by the size of the buffer
  do {
    // Calculate position (either count from the beginning or from the end)
    if (!from_top) {
      pos -= BUFSIZ;
      if (pos < start_pos)
        pos = start_pos;
      fseek(f, pos, SEEK_SET);
    }
    char_num = fread(buffer, 1, BUFSIZ, f);

    // Count number of newlines in the buffer and possibly find the right line
    i = 0;
    while (from_top ? i < char_num : char_num > 0) {
      if (from_top)
        c = buffer[i];
      else
        c = buffer[char_num];

      if (c == '\n')
        lines--;

      // Found the line
      if (from_top ? lines < 1 : lines < 0) {
        if (from_top)
          pos += i + 1;
        else
          pos += char_num + 1;
        print_from_position(f, &pos);
        return EXIT_SUCCESS;
      }

      if (from_top)
        i++;
      else
        char_num--;
    }

    // If requested amount of lines is bigger than number of lines in the
    // document -> print everything
    if (!from_top && pos == start_pos && lines > 0) {
      pos = start_pos;
      print_from_position(f, &pos);
      return EXIT_SUCCESS;
    }

    if (from_top && pos == end_pos && lines > 0) {
      return EXIT_SUCCESS;
    }

    if (from_top)
      pos += char_num;
  } while (pos != end_pos);

  return EXIT_FAILURE;
}

void print_usage()
{
  fprintf(stderr, "Usage: %s [-n lines] filename\n", PROGRAM_NAME);
}

void parse_options(int argc, char **argv, int *lines, bool *from_top)
{
  int opt;
  char *endptr;
  while((opt = getopt(argc, argv, "n:")) != -1) {
    switch(opt) {
    case 'n':
      if (*optarg == '+')
        *from_top = true;
      else if (*optarg == '-')
        optarg++;

      *lines = (int) strtol(optarg, &endptr, 10);
      if (*endptr != '\0')
        error(EXIT_FAILURE, 0, "invalid number of lines: '%s'", optarg);
      break;
    default:
      print_usage();
      exit(EXIT_FAILURE);
      break;
    }
  }
}

int main (int argc,char *argv[])
{
  int lines = DEFAULT_N_LINES;
  bool from_top = false;
  char *filename;

  parse_options(argc, argv, &lines, &from_top);

  // Get the filename from the first argument
  if (optind < argc)
    filename = argv[optind];
  else
    filename = "-";

  // If there's nothing to print, then exit
  if (!from_top && lines == 0)
    return EXIT_SUCCESS;

  FILE *f;
  if (strcmp(filename, "-") == 0)
    f = stdin;
  else
    f = fopen(filename, "r");

  if (f == NULL) {
    error(EXIT_FAILURE, 0, "cannot open '%s' for reading", filename);
  }

  return tail_file(f, lines, from_top);
}
